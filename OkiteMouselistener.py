import struct
import OkiteConfig
from threading import Thread

# code from http://stackoverflow.com/a/16682549

CLICK_EVENT_FORMAT = 'llHHI'
CLICK_EVENT_SIZE = struct.calcsize(CLICK_EVENT_FORMAT)


class OkiteMouseListener(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.click_fired = False
        self.running = True

    def has_triggered(self):
        return self.click_fired

    def run(self):
        try:
            click_handle = open(OkiteConfig.CLICK_DEVICE, "rb")
            event = click_handle.read(CLICK_EVENT_SIZE)

            while event and self.running:
                (_, _, type, code, value) = struct.unpack(CLICK_EVENT_FORMAT, event)

                if (
                    type == OkiteConfig.CLICK_TYPE and
                    code == OkiteConfig.CLICK_CODE and
                    value == OkiteConfig.CLICK_VALUE
                ):
                    self.click_fired = True
                    break

                event = click_handle.read(CLICK_EVENT_SIZE)
            click_handle.close()
        except:
            print "failed to access %s" % OkiteConfig.CLICK_DEVICE
            print "mouse click listening disabled"
            self.click_fired = False

    def stop(self):
        self.running = False
