from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean
)


class Alarm(Base):

    __tablename__ = 'okite_alarms'

    # internal ID
    ID = Column(Integer, nullable=False, primary_key=True)

    # String containing hour and minute of the alarm
    # e.g. "1230" = 12:30 (24h format)
    time = Column(Integer)

    # a string containing any of substrings from WEEKDAYS
    # (without spaces), e.g. "MOTHSA"
    days = Column(String(14))

    # if set to False, alarm will still be kept in the database
    # but will be inactive
    enabled = Column(Boolean, unique=False, default=True)

    # internal lock that is set in atomic manner
    # if the entry is currently processed by the alarm timer
    lock = Column(Boolean, unique=False, default=False)

    # if set to True the alarm will be repeated weekly
    repeat = Column(Boolean, unique=False, default=False)

    # ID that specifies the AudioProfile entry
    audio_id = Column(Integer)

    # DDMMYYYY representation of the last successful alarm
    # e.g. "02012015" = 2. Jan. 2015
    last_alarm = Column(String)


class AudioProfile(Base):

    __tablename__ = 'okite_audio'

    # internal ID
    ID = Column(Integer, nullable=False, primary_key=True)

    # human readable identifier for display purposes
    name = Column(String(255))

    # duration in seconds of volume increase 0% to 100%
    inc_dur = Column(Integer)

    # amounts of looping if sound has finished
    # <0 = infinite
    # =0 = no loop
    # >0 = loops equal to specified number
    loops = Column(Integer)

    # path to
    # a) directory; random file will be played
    # b) audio file: specified file will be played
    path = Column(String(255))
