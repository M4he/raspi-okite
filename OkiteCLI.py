import os
import sys
from OkiteCore import OkiteCore, WEEKDAYS

core = OkiteCore()

yes = set(['yes', 'y', 'ye'])


def parse_cmd():
    if sys.argv[1] in ['alarm', 'alarms']:
        if len(sys.argv) < 3:
            print "ERROR: too few arguments"
            exit()
        _chain_alarms(sys.argv[2:])
    elif sys.argv[1] in ['profile', 'profiles', 'audio']:
        if len(sys.argv) < 3:
            print "ERROR: too few arguments"
            exit()
        _chain_audio(sys.argv[2:])
    elif sys.argv[1] in ['run', 'exec']:
        _exec_alarms()
    else:
        print "ERROR: invalid command"


def _exec_alarms():
    core.execute_alarms()


#############################
# ALARM COMMAND SUB-CHAIN ##
###########################

def _chain_alarms(args):
    cmd = args[0]

    if cmd == 'get':
        _cmd_get_alarms()

    elif cmd == 'mod':
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        alarms = core.get_alarms()
        ID = args[1]
        mod_alarm = None
        for alarm in alarms:
            if str(alarm.ID) == ID:
                mod_alarm = alarm
                break
        if mod_alarm is None:
            print "ERROR: alarm with ID %s not found!" % ID
            exit()
        _cmd_mod_alarm(mod_alarm)

    elif cmd == 'del':
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        alarms = core.get_alarms()
        ID = args[1]
        del_alarm = None
        for alarm in alarms:
            if str(alarm.ID) == ID:
                del_alarm = alarm
                break
        if del_alarm is None:
            print "ERROR: alarm with ID %s not found!" % ID
            exit()
        _cmd_del_alarm(del_alarm)

    elif cmd in ['enable', 'on']:
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        alarms = core.get_alarms()
        ID = args[1]
        on_alarm = None
        for alarm in alarms:
            if str(alarm.ID) == ID:
                on_alarm = alarm
                break
        if on_alarm is None:
            print "ERROR: alarm with ID %s not found!" % ID
            exit()
        _cmd_enable_alarm(on_alarm)

    elif cmd in ['disable', 'off']:
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        alarms = core.get_alarms()
        ID = args[1]
        off_alarm = None
        for alarm in alarms:
            if str(alarm.ID) == ID:
                off_alarm = alarm
                break
        if off_alarm is None:
            print "ERROR: alarm with ID %s not found!" % ID
            exit()
        _cmd_disable_alarm(off_alarm)

    elif cmd == 'add':
        _cmd_add_alarm()


def _cmd_add_alarm():
    time = _input_time()
    days = _input_days()
    repeat = _input_repeat()
    audio_id = _input_profile()
    params = {
        'time': time,
        'days': days,
        'repeat': repeat,
        'audio_id': audio_id
    }
    alarm_id = core.add_alarm(params)
    print
    print "Alarm with ID %s created." % alarm_id


def _cmd_enable_alarm(alarm):
    params = {
        'enabled': True
    }
    core.modify_alarm(alarm.ID, params)
    print
    print "Alarm has been enabled:"
    _get_and_print_alarm(alarm.ID)


def _cmd_disable_alarm(alarm):
    params = {
        'enabled': False
    }
    core.modify_alarm(alarm.ID, params)
    print
    print "Alarm has been disabled:"
    _get_and_print_alarm(alarm.ID)


def _cmd_mod_alarm(alarm):
    time = _input_time(default=alarm.time)
    days = _input_days(default=alarm.days)
    repeat = _input_repeat(default=alarm.repeat)
    audio_id = _input_profile(default=alarm.audio_id)
    params = {
        'time': time,
        'days': days,
        'repeat': repeat,
        'audio_id': audio_id
    }
    core.modify_alarm(alarm.ID, params)
    print
    print "Alarm has been modified:"
    _get_and_print_alarm(alarm.ID)


def _cmd_del_alarm(alarm):
    print
    _get_and_print_alarm(alarm.ID)
    print
    print "Delete alarm with ID %s (y/n)?" % str(alarm.ID)
    choice = raw_input().lower()
    if choice in yes:
        core.remove_alarm(alarm.ID)
        print
        print "Alarm deleted."
    else:
        print
        print "Aborted."
        exit()


def _cmd_get_alarms():
    alarms = core.get_alarms()
    for alarm in alarms:
        _print_alarm(alarm)


def _get_and_print_alarm(alarm_id):
    alarms = core.get_alarms()
    found = False
    for alarm in alarms:
        if alarm.ID == alarm_id:
            _print_alarm(alarm)
            found = True
            break
    if not found:
        print "ERROR: internal error, alarm with ID %s was not found in the system!" % alarm_id


def _print_alarm(alarm):
    tempstr = str(alarm.time).zfill(4)
    timestr = "%s:%s" % (tempstr[:2], tempstr[2:])
    repeatstr = (alarm.repeat is True) and "YES" or "NO"
    enabledstr = (alarm.enabled is True) and ":" or ": [DISABLED]"
    output = (
        "ID %s%s %s at %s (repeat: %s), "
        "audio profile %s (last execution %s)" % (
            str(alarm.ID), enabledstr, str(alarm.days), timestr,
            repeatstr, str(alarm.audio_id), alarm.last_alarm
        )
    )
    print output


#############################
# AUDIO COMMAND SUB-CHAIN ##
###########################

def _chain_audio(args):
    cmd = args[0]

    if cmd == 'get':
        _cmd_get_profiles()

    elif cmd == 'mod':
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        profiles = core.get_audio_profiles()
        ID = args[1]
        mod_profile = None
        for profile in profiles:
            if str(profile.ID) == ID:
                mod_profile = profile
                break
        if mod_profile is None:
            print "ERROR: audio profile with ID %s not found!" % ID
            exit()
        _cmd_mod_profile(mod_profile)

    elif cmd == 'del':
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        profiles = core.get_audio_profiles()
        ID = args[1]
        del_profile = None
        for profile in profiles:
            if str(profile.ID) == ID:
                del_profile = profile
                break
        if del_profile is None:
            print "ERROR: audio profile with ID %s not found!" % ID
            exit()
        _cmd_del_profile(del_profile)

    elif cmd == 'add':
        _cmd_add_profile()

    elif cmd == 'test':
        if len(args) < 2 or not args[1].isdigit():
            print "ERROR: ID not specified or invalid"
            exit()
        profiles = core.get_audio_profiles()
        ID = args[1]
        test_profile = None
        for profile in profiles:
            if str(profile.ID) == ID:
                test_profile = profile
                break
        if test_profile is None:
            print "ERROR: audio profile with ID %s not found!" % ID
            exit()
        _cmd_test_profile(test_profile)


def _cmd_add_profile():
    name = _input_name()
    inc_dur = _input_incdur()
    loops = _input_loops()
    path = _input_path()
    params = {
        'name': name,
        'inc_dur': inc_dur,
        'loops': loops,
        'path': path
    }
    profile_id = core.add_audio_profile(params)
    print
    print "Audio profile with ID %s created." % profile_id


def _cmd_mod_profile(audio):
    name = _input_name(default=audio.name)
    inc_dur = _input_incdur(default=audio.inc_dur)
    loops = _input_loops(default=audio.loops)
    path = _input_path(default=audio.path)
    params = {
        'name': name,
        'inc_dur': inc_dur,
        'loops': loops,
        'path': path
    }
    core.modify_audio_profile(audio.ID, params)
    print
    print "Audio profile has been modified:"
    _get_and_print_profile(audio.ID)


def _cmd_del_profile(profile):
    for alarm in core.get_alarms():
        if str(profile.ID) == str(alarm.audio_id):
            print "ERROR: audio profile in use by alarm with ID %s" % str(alarm.ID)
            exit()
    print
    _get_and_print_profile(profile.ID)
    print
    print 'Delete audio profile "%s" with ID %s (y/n)?' % (profile.name, str(profile.ID))
    choice = raw_input().lower()
    if choice in yes:
        core.remove_audio_profile(profile.ID)
        print
        print "Profile deleted."
    else:
        print
        print "Aborted."
        exit()


def _cmd_get_profiles():
    profiles = core.get_audio_profiles()
    for profile in profiles:
        _print_profile(profile)


def _cmd_test_profile(profile):
    core.test_audio_profile(profile.ID)


def _get_and_print_profile(profile_id):
    profiles = core.get_audio_profiles()
    found = False
    for profile in profiles:
        if profile.ID == profile_id:
            _print_profile(profile)
            found = True
            break
    if not found:
        print "ERROR: internal error, audio profile with ID %s was not found in the system!" % profile_id


def _list_profiles():
    profiles = core.get_audio_profiles()
    output = "Available = "
    has_entry = False
    for profile in profiles:
        if has_entry:
            output += ", "
        output += str(profile.ID) + ": " + profile.name
        has_entry = True
    print output


def _print_profile(profile):
    output = (
        'ID %s: "%s", %s loops, vol+ for %ss, path: %s' % (
            str(profile.ID), profile.name, str(profile.loops),
            str(profile.inc_dur), profile.path
        )

    )
    print output


###################
# INPUT METHODS ##
#################

## ALARM INPUT METHODS

def _input_time(default=None):
    while True:
        if default is None:
            print
            print "Input clock time (HH:MM or H:MM):"
        else:
            timestr = str(default).zfill(4)
            timestr = timestr[:2] + ':' + timestr[-2:]
            print
            print "Input clock time [%s]:" % timestr
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate    HH:MM     or     H:MM
        if len(choice) > 5 or len(choice) < 4:
            continue
        if ':' not in choice:
            continue
        hours = choice[:2]

        # compatibility with single digit hours
        if ':' in hours:
            hours = hours[:1]

        minutes = choice[-2:]
        if not hours.isdigit() or int(hours) > 23:
            continue
        if not minutes.isdigit() or int(minutes) > 59:
            continue
        return int(str(hours) + str(minutes))


def _input_days(default=None):
    while True:
        if default is None:
            print
            print "Input days ({DD}* without spaces):"
        else:
            print
            print "Input days [%s]:" % default
        choice = raw_input().upper()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 2:
            continue
        if len(choice) % 2 > 0:
            continue
        days_valid = True
        for i in range(0, len(choice), 2):
            day = choice[i:i+2]
            if not day in WEEKDAYS:
                days_valid = False
        if not days_valid:
            continue

        return choice


def _input_repeat(default=None):
    while True:
        if default is None:
            print
            print "Repeat weekly (1/0)?"
        else:
            print
            print "Repeat weekly [%s]?" % str(default)
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if not choice in ['1', '0']:
            continue

        return (choice == '1')


def _input_profile(default=None):
    while True:
        if default is None:
            print
            print "Enter audio profile ID:"
            print
            _list_profiles()
        else:
            print
            print "Enter audio profile ID [%s]:" % str(default)
            print
            _list_profiles()
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 1:
            continue
        if not choice.isdigit():
            continue
        profile_found = False
        for profile in core.get_audio_profiles():
            if str(profile.ID) == choice:
                profile_found = True
                break
        if not profile_found:
            print "ERROR: an audio profile with ID %s does not exist!" % choice
            continue

        return int(choice)


## AUDIO PROFILE INPUT METHODS

def _input_name(default=None):
    while True:
        if default is None:
            print
            print "Enter profile name:"
        else:
            print
            print "Enter profile name [%s]:" % default
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 1:
            continue
        profile_found = False
        for profile in core.get_audio_profiles():
            if str(profile.name) == choice:
                profile_found = True
                break
        if profile_found:
            print "ERROR: an audio profile with that name already exists!"
            continue

        return choice


def _input_incdur(default=None):
    while True:
        if default is None:
            print
            print "Enter duration of volume increase in seconds (0 = off):"
        else:
            print
            print "Enter duration of volume increase in seconds [%s]:" % str(default)
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 1:
            continue
        if not choice.isdigit():
            continue
        if int(choice) < 0:
            continue

        return int(choice)


def _input_loops(default=None):
    while True:
        if default is None:
            print
            print "Enter amount of loops (<0 = infinite):"
        else:
            print
            print "Enter amount of loops [%s]:" % str(default)
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 1:
            continue
        try:
            int(choice)
        except:
            continue

        return int(choice)


def _input_path(default=None):
    while True:
        if default is None:
            print
            print "Enter audio file path/dir:"
        else:
            print
            print "Enter audio file path/dir [%s]:" % default
        choice = raw_input()
        if choice == '' and default is not None:
            return default

        # validate
        if len(choice) < 1:
            continue
        if not (os.path.isfile(choice) or os.path.isdir(choice)):
            print "ERROR: file or directory not found"
            continue

        return choice


##########
# MAIN ##
########

parse_cmd()
