import os
import subprocess
import pygame
import time
import threading
import OkiteConfig
from random import randint
from OkiteMouselistener import OkiteMouseListener

SUPPORTED_FORMATS = ['.wav', '.mp3', '.ogg']
DEFAULT_SOUND = 'default_alarm.wav'


class OkitePlayer(object):

    def __init__(self, path, inc_dur, loops):
        self.path = path
        self.inc_dur = inc_dur
        self.loops = int(loops)
        try:
            limit_minutes = int(OkiteConfig.MAX_ALARM_PLAYBACK_MINUTES)
        except:
            # default if parsing fails
            limit_minutes = 60
        self.play_limit_seconds = float(limit_minutes * 60)

        try:
            use_click = OkiteConfig.CLICK_ENABLED
        except:
            # default if parsing fails
            use_click = True
        self.click_enabled = use_click

    def run(self):
        self.thread = threading.Thread(target=self.play, args=())
        self.thread.start()

    def play(self):
        self._ensure_audio()
        if os.path.isdir(self.path):
            self._play_dir()
        elif os.path.isfile(self.path):
            self._play_file()
        else:
            self._play_default_sound()

    def _play_dir(self):
        files = self._list_audio_files(self.path)
        num_files = len(files)
        if num_files == 0:
            self._play_default_sound()
        else:
            chosen_index = randint(0, num_files-1)
            sound = files[chosen_index]
            self.path = sound
            self._play_file()

    def _play_file(self):
        print "Playing %s" % self.path
        inc_dur = float(self.inc_dur)
        loop_arg = (self.loops < 0) and -1 or self.loops

        if OkiteConfig.CLICK_ENABLED:
            mouse = OkiteMouseListener()
            mouse.start()

        play_limit = self.play_limit_seconds

        pygame.mixer.init()
        pygame.mixer.music.load(self.path)
        pygame.mixer.music.set_volume(0.0)
        pygame.mixer.music.play(loop_arg)
        seconds_passed = 0.0
        vol = 0.0
        clk = time.time()
        if inc_dur <= 0.0:
            vol = 1.0
        while pygame.mixer.music.get_busy():

            # calculate time delta
            now = time.time()
            delta = now - clk
            clk = now
            seconds_passed += delta

            # slowly increase volume
            if vol < 1.0:
                vol_inc = delta/inc_dur
                vol += vol_inc
                if vol > 1.0:
                    vol = 1.0
                pygame.mixer.music.set_volume(vol)

            if self.click_enabled and mouse.has_triggered():
                print "mouse click event registered, stopping playback"
                pygame.mixer.music.stop()
                break

            if seconds_passed > play_limit:
                print "time limit reached, stopping playback"
                pygame.mixer.music.stop()
                break

            time.sleep(0.01)

        if self.click_enabled:
            mouse.stop()
            mouse.join()
        print "player ended after %s seconds" % str(seconds_passed)

    def _play_default_sound(self):
        self.inc_dur = 0
        self.path = DEFAULT_SOUND
        self._play_file()

    def _ensure_audio(self):
        # switch output to 3.5mm jack
        subprocess.call([
            'amixer',
            'cset',
            'numid=3',
            '1'
        ])
        # ensure it is unmuted
        subprocess.call([
            'amixer',
            'cset',
            'numid=4',
            'on'
        ])
        # set volume to 100%
        subprocess.call([
            'amixer',
            'cset',
            'numid=1',
            '100%'
        ])

    def _list_audio_files(self, dirpath):
        result = []
        for f in os.listdir(dirpath):
            if f.lower().endswith(tuple(SUPPORTED_FORMATS)):
                result.append(os.path.join(dirpath, f))
        return result
