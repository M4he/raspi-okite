#!/bin/bash
#
# get absolute script path
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
echo "Deploying from $SCRIPTPATH"
#
# create target dir if non-existent
if [ ! -d "/opt/okite" ]; then
    sudo mkdir -p /opt/okite
fi
sudo chmod o+rw /opt/okite
#
# create database file if non-existent
if [ ! -f "/opt/okite/okite.db" ]; then
    sudo touch /opt/okite/okite.db
    # set read-write permission for everyone
    sudo chmod o+rw /opt/okite/okite.db
fi
#
# create log file if non-existent
if [ ! -f "/opt/okite/okite.log" ]; then
    sudo touch /opt/okite/okite.log
    # set read-write permission for everyone
    sudo chmod o+rw /opt/okite/okite.log
fi
#
# copy over source code
sudo cp $SCRIPTPATH/*.py /opt/okite/.
#
# change source code's ownership to root
sudo chown root:root /opt/okite/*.py
# prevent code modification through other users
sudo chmod o-w /opt/okite/*.py
# allow other users to access the code files
sudo chmod o+r /opt/okite/*.py