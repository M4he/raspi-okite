# raspi-okite
Alarm clock functionality for the Raspberry Pi using the audio jack, featuring audio profiles with volume increase and loop settings as well as multiple, weekday-specific alarms.

**THIS IS A WORK-IN-PROGRESS**, nothing in this repository is thoroughly tested!

## PLEASE NOTE (WIP)
The following features are still missing or unfinished:
- adding compatibility for other audio outputs
    - currently, okite will:
        - forcibly turn on the 3.5 mm audio jack
        - set it as default output
        - unmute it and set its volume to 100%
    - this is to ensure the alarm will play no matter what
- making the logging more sophisticated
- providing a nice web GUI accessible by browser
    - this would consist of:
        - JSON-API via vhost server process
        - HTML+JS+CSS3 page interacting with API
    - this is a huge task for me and is not expected to be done anytime soon

## Requirements
`python-sqlalchemy`
`python-pygame`

## Installing / Updating
Checkout the code:
```
git clone https://github.com/M4he/raspi-okite.git
```
Execute the bundled bash script:
```
chmod +x raspi-okite/deploy.sh
raspi-okite/deploy.sh
```
If it doesn't work, make sure you have the package `realpath` installed! (`sudo apt-get install realpath`)

This will:
- create `/opt/okite/` if non-existent
- move the Python scripts into that dir
- create necessary database and log files
- adjust permissions for
    - Python scripts to make them read-only by non-root users
    - database file and log file to be writeable by everyone


## Enabling
Add the script to crontab, to be executed every minute:
```
sudo crontab -e

    * * * * * python /opt/okite/OkiteCLI.py run
```
**IMPORTANT: make sure there is a linebreak after the last entry in crontab!**  
**NOTE: if you choose not to put it into root's crontab, be aware that the mouse click interrupt for alarms won't work! (script needs access to /dev/input/..)**

the `run` argument will instruct the script to check the database for alarms and executes them if applicable.

## Command Line Interface (CLI) 
Executed via `python /opt/okite/OkiteCLI.py <arguments>`

### Arguments

| Arguments | Description |
|---|---|
| `run` | checks for due alarms and executes them |
| `alarms get` | displays all alarms |
| `alarms del <id>` | attempts to remove the alarm identified by `id` |
| `alarms mod <id>` | modifies alarm identified by `id` (interactive) |
| `alarms on <id>` | enables an alarm identified by `id` |
| `alarms off <id>` | disables an alarm identified by `id` |
| `alarms add` | creates a new alarm (interactive) |
| `audio get` | displays all audio profiles |
| `audio del <id>` | attempts to remove the audio_profile identified by `id` |
| `audio mod <id>` | modifies audio_profile identified by `id` (interactive) |
| `audio add` | creates a new audio_profile (interactive) |
| `audio test <id>` | Runs a test playback for audio_profile identified by `id` as if the alarm is going off |

The following arguments are synonyms:
- `run` and `exec`
- `alarm` and `alarms`
- `on` and `enable`, `off` and `disable`
- `audio`, `profile` and `profiles`

#### Turn the CLI into a command
Create a file `/usr/bin/okite` with the following contents:
```
#!/bin/bash
sudo python /opt/okite/OkiteCLI.py "$@"
```
Make it executable via `sudo chmod +x /usr/bin/okite`

You can now use the CLI via the `okite` command, e.g.:
```
okite alarms get
```

## Sample Setup

```
# first, create an audio profile
okite audio add

    Enter profile name:
    MyAlarm

    Enter duration of volume increase in seconds (0 = off):
    360

    Enter amount of loops (<0 = infinite):
    10

    Enter audio file path/dir:
    /home/pi/alarms/goodmorning.mp3

# check if the added entry is there
# for a clean setup this should have ID 1
okite audio get

    ID 1: "MyAlarm", 10 loops, vol+ for 360s, path: /home/pi/alarms/goodmorning.mp3


# test your newly created audio
okite audio test 1


# add an alarm, use the audio ID from above
okite alarm add

    Input clock time (HH:MM):
    07:30

    Input days ({DD}* without spaces):
    MOFRSU

    Repeat weekly (1/0)?
    0

    Enter audio profile ID:
    1
```
You are good to go. You can check if okite is checking for due alarms if you observe `/opt/okite/okite.log`, every alarm check is noted there.

## Disclaimer
This software comes with no guarantee / warranty!
If you are late to work because the alarm via okite didn't go off, I'm not responsible :) However since I'm using this as an alarm clock myself, I try my best to make this tool as reliable as possible!