DATABASE_PATH = 'sqlite:////opt/okite/okite.db'
LOG_PATH = '/opt/okite/okite.log'
LOG_MAX_BYTES = 1000000
LOG_BACKUP_COUNT = 2
#
# time in minutes after the alarm will stop playing automatically
MAX_ALARM_PLAYBACK_MINUTES = 60
#
# enables mouse click interrupt for alarms
CLICK_ENABLED = True
# EVDEV mouse click values, YMMV
CLICK_DEVICE = "/dev/input/event0"
CLICK_TYPE = 1
CLICK_CODE = 272
CLICK_VALUE = 1
