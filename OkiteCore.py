import time
import datetime
import logging
from logging.handlers import RotatingFileHandler
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from OkitePlayer import OkitePlayer
from OkiteClasses import Base, Alarm, AudioProfile
import OkiteConfig

WEEKDAYS = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU']


class OkiteCore(object):

    def __init__(self):
        self._init_logging()
        self._init_db_engine()
        self.log.info('OkiteCore started')

    def get_alarms(self):
        session = self.session
        results = session.query(Alarm).filter(
            Alarm.lock == False
        ).all()
        return results

    def add_alarm(self, params):
        session = self.session
        datenow = datetime.datetime.now()
        now_day_str = self._date_to_daystr(datenow)
        now_time_int = self._generate_time_int(datenow)
        now_date_stamp = self._generate_datestamp(datenow)

        # if the alarm includes today and the alarm time
        # is in the past, set 'last_alarm' to today to
        # prevent the alarm going off shortly after adding it
        if (
            now_day_str in params['days'] and
            params['time'] < now_time_int
        ):
            last_alarm = now_date_stamp
        else:
            last_alarm = '00000000'

        alarm = Alarm(
            time=params['time'],
            days=params['days'],
            repeat=params['repeat'],
            lock=False,
            audio_id=params['audio_id'],
            last_alarm=last_alarm
        )
        session.add(alarm)
        session.commit()
        self.log.info('inserted alarm with id %s' % alarm.ID)
        return alarm.ID

    def modify_alarm(self, alarm_id, params):
        session = self.session
        datenow = datetime.datetime.now()
        now_day_str = self._date_to_daystr(datenow)
        now_time_int = self._generate_time_int(datenow)
        now_date_stamp = self._generate_datestamp(datenow)

        alarm = session.query(Alarm).filter(
            Alarm.ID == alarm_id
        ).first()

        # only set values which are provided by params and
        # match any (non-method) alarm attribute
        for attr in dir(alarm):
            if not attr.startswith('__') and not callable(getattr(alarm, attr)):
                if attr in params:
                   setattr(alarm, attr, params[attr])

        # if the alarm includes today and the alarm time
        # is in the past, set 'last_alarm' to today to
        # prevent the alarm going off shortly after modifying it
        if (
            now_day_str in alarm.days and
            alarm.time < now_time_int
        ):
            alarm.last_alarm = now_date_stamp

        session.commit()

    def remove_alarm(self, alarm_id):
        session = self.session
        session.delete(
            session.query(Alarm).filter(
                Alarm.ID == alarm_id
            ).first()
        )
        session.commit()

    def get_audio_profiles(self):
        session = self.session
        results = session.query(AudioProfile).all()
        return results

    def add_audio_profile(self, params):
        session = self.session
        profile = AudioProfile(
            name=params['name'],
            path=params['path'],
            inc_dur=params['inc_dur'],
            loops=params['loops']
        )
        session.add(profile)
        session.commit()
        self.log.info('inserted audio_profile with id %s' % profile.ID)
        return profile.ID

    def modify_audio_profile(self, profile_id, params):
        session = self.session
        profile = session.query(AudioProfile).filter(
            AudioProfile.ID == profile_id
        ).first()

        # only set values which are provided by params and
        # match any (non-method) profile attribute
        for attr in dir(profile):
            if not attr.startswith('__') and not callable(getattr(profile, attr)):
                if attr in params:
                    setattr(profile, attr, params[attr])

        session.commit()

    def remove_audio_profile(self, profile_id):
        session = self.session
        session.delete(
            session.query(AudioProfile).filter(
                AudioProfile.ID == profile_id
            ).first()
        )
        session.commit()

    def test_audio_profile(self, profile_id):
        self._spawn_player(profile_id)

    # will check for pending alarms and execute those
    # which are due
    def execute_alarms(self):
        alarms = self._get_eligible_alarms()
        for alarm in alarms:
            self._execute_alarm(alarm)

    # TODO: safety check: if audio_id doesn't exist
    # fallback to default alarm
    def _execute_alarm(self, alarm):
        self.log.info('executing alarm with id %s' % alarm.ID)
        session = self.session
        alarm.lock = True
        session.commit()
        audio_id = alarm.audio_id
        self._spawn_player(audio_id)
        if alarm.repeat:
            datenow = datetime.datetime.now()
            alarm.last_alarm = self._generate_datestamp(datenow)
            alarm.lock = False
        else:
            session.delete(alarm)
        session.commit()

    def _spawn_player(self, audio_id):
        self.log.info('spawning player for audio id %s' % audio_id)
        session = self.session
        audio_profile = session.query(AudioProfile).filter(
            AudioProfile.ID == audio_id
        ).first()
        OkitePlayer(
            audio_profile.path,
            audio_profile.inc_dur,
            audio_profile.loops
        ).run()

    def _get_eligible_alarms(self):
        session = self.session
        datenow = datetime.datetime.now()
        now_day_str = self._date_to_daystr(datenow)
        now_date_stamp = self._generate_datestamp(datenow)
        now_time_int = self._generate_time_int(datenow)

        time_start = time.time()

        results = session.query(Alarm).filter(
            Alarm.lock == False
        ).filter(
            Alarm.enabled == True
        ).filter(
            # alarms set for today's day-of-week
            Alarm.days.like('%' + now_day_str + '%')
        ).filter(
            # clock time is now or has already passed
            Alarm.time <= now_time_int
        ).filter(
            # exclude already executed alarms
            Alarm.last_alarm != now_date_stamp
        ).all()

        time_passed = time.time() - time_start

        self.log.info('found %s eligible alarms after %s seconds' % (len(results), time_passed) )
        return results

    def _generate_datestamp(self, datime):
        return datime.strftime('%d%m%Y')

    def _generate_time_int(self, dat):
        timestr = dat.strftime('%H%M')
        return int(timestr)

    def _date_to_daystr(self, dat):
        return WEEKDAYS[dat.weekday()]

    def _init_db_engine(self):
        engine = create_engine(OkiteConfig.DATABASE_PATH)
        Base.metadata.create_all(engine)
        Base.metadata.bind = engine
        DBSessionClass = sessionmaker(bind=engine)
        self.session = DBSessionClass()

    def _init_logging(self):
        self.log = logging.getLogger("okite_logger")
        self.log.setLevel(logging.INFO)
        handler = RotatingFileHandler(
            OkiteConfig.LOG_PATH,
            maxBytes=OkiteConfig.LOG_MAX_BYTES,
            backupCount=OkiteConfig.LOG_BACKUP_COUNT
        )
        self.log.addHandler(handler)
